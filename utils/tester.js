workercode = "worker.js";

function executeCode( code, callback,...inputValues ) {
    var timeout;

    code = code + `main(${inputValues.map((mapValue)=>{
        return typeof mapValue == "string" ? `'`+mapValue+`'`:mapValue;  
    }).toString()})`;
    var worker = new Worker( workercode );

    worker.addEventListener( "message", function(event) {
        clearTimeout(timeout);
        callback( event.data );
    });

    worker.postMessage({
        code: code
    });

    timeout = window.setTimeout( function() {
        callback( "Maximum execution time exceeded" );
        worker.terminate();
    }, 4000 );
}

function test(code,test){
    return new Promise((resolve,reject)=>{
        executeCode(code,(answer)=>{
            let result = undefined;
            if(answer === test.outputValue){
                result = true;
            }else{
                result = false;
            }
            resolve(result);
        },...test.inputValue);
    })
    
}


module.exports = test;
